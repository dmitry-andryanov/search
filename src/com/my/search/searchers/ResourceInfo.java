package com.my.search.searchers;

public class ResourceInfo {
    private String url;
    private String shortDescription;

    public String getUrl() {
        return url;
    }

    public String getShortDescription() {
        return shortDescription;
    }
}
