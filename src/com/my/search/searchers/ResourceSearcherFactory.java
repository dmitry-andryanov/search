package com.my.search.searchers;

public interface ResourceSearcherFactory {
    ResourceSearcher getSearcher(String language);
}
