package com.my.search.searchers;

import java.util.List;

/**
 * Base interface for all searchers
 */
public interface ResourceSearcher {
    /**
     * Get resource info with url and short description from list of stings in search query
     *
     * @param strings string for search query
     * @return list of resource info
     */
    List<ResourceInfo> getResourceListForString(List<String> strings);
}
