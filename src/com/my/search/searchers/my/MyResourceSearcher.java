package com.my.search.searchers.my;

import com.my.search.searchers.ResourceInfo;
import com.my.search.searchers.ResourceSearcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class MyResourceSearcher implements ResourceSearcher {
    @Override
    public List<ResourceInfo> getResourceListForString(List<String> words) {
        Map<String, List<ResourceInfo>> availableResources = getAvailableResources();
        Map<ResourceInfo, Long> entries = words.stream()
            .map(availableResources::get)
            .filter(Objects::nonNull)
            .flatMap(List::stream)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        List<Map.Entry<ResourceInfo, Long>> list = new ArrayList<>(entries.entrySet());
        list.sort(Map.Entry.comparingByValue());
        List<ResourceInfo> result = new ArrayList<>();
        for (Map.Entry<ResourceInfo, Long> resourceInfoLongEntry : list) {
            result.add(resourceInfoLongEntry.getKey());
        }
        return result;
    }

    protected abstract Map<String, List<ResourceInfo>> getAvailableResources();
}
