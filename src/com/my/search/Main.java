package com.my.search;

import com.my.search.parsers.my.MyStringToWordsFactory;
import com.my.search.parsers.StringToWordsFactory;
import com.my.search.parsers.StringToWordsParser;
import com.my.search.searchers.ResourceInfo;
import com.my.search.searchers.ResourceSearcher;
import com.my.search.searchers.ResourceSearcherFactory;
import com.my.search.searchers.my.MyResourceSearcherFactory;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        StringToWordsFactory parserFactory = new MyStringToWordsFactory();
        String language = "en";
        StringToWordsParser enParser = parserFactory.getParser(language);

        List<String> wordsFromString = enParser.getWordsFromString("Some words");

        ResourceSearcherFactory searcherFactory = new MyResourceSearcherFactory();
        ResourceSearcher searcher = searcherFactory.getSearcher(language);

        List<ResourceInfo> resourceListForWords = searcher.getResourceListForString(wordsFromString);
    }
}
