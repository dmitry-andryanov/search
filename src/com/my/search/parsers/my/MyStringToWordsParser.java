package com.my.search.parsers.my;

import com.my.search.parsers.StringToWordsParser;

import java.util.Arrays;
import java.util.List;

/**
 * Base parser logic based on removing all special symbols, replacing them by space symbols and then split string by space symbol
 */
public abstract class MyStringToWordsParser implements StringToWordsParser {
    private static final String SPACE = " ";
    private static final String REPEATABLE_SPACES_REGEXP = " ";

    @Override
    public List<String> getWordsFromString(String words) {
        String afterRemoveAllSpecialSymbols = words.replaceAll(getRegExpForSpecialSymbols(), SPACE);
        String afterRemoveAllRepeatableSpaces = afterRemoveAllSpecialSymbols.replaceAll(REPEATABLE_SPACES_REGEXP, SPACE);

        return Arrays.asList(afterRemoveAllRepeatableSpaces.split(SPACE));
    }

    protected abstract String getRegExpForSpecialSymbols();
}
