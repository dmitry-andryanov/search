package com.my.search.parsers.my;

import com.my.search.parsers.my.MyStringToWordsParser;

public class EnglishStringToWordsParser extends MyStringToWordsParser {
    @Override
    protected String getRegExpForSpecialSymbols() {
        return regExpForEnglish;
    }
}
