package com.my.search.parsers.my;

import com.my.search.parsers.StringToWordsFactory;
import com.my.search.parsers.StringToWordsParser;

import java.util.HashMap;
import java.util.Map;

public class MyStringToWordsFactory implements StringToWordsFactory {
    /**
     * Map with all parsers with key language
     */
    private static final Map<String, MyStringToWordsParser> ALL_PARSERS = new HashMap<>();

    public MyStringToWordsFactory() {
        //init ALL_PARSERS map
        ALL_PARSERS.put("en", new EnglishStringToWordsParser());
    }

    @Override
    public StringToWordsParser getParser(String language) {
        return ALL_PARSERS.get(language);
    }
}
