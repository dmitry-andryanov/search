package com.my.search.parsers;

import java.util.List;

/**
 * Interface for parsing words from string. Word is symbol sequence from language without special symbols and numbers
 */
public interface StringToWordsParser {
    /**
     * Divide string with words into list of words
     *
     * @param words string with words
     * @return list of words
     */
    List<String> getWordsFromString(String words);
}
