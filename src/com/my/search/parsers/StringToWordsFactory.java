package com.my.search.parsers;

public interface StringToWordsFactory {
    StringToWordsParser getParser(String language);
}
